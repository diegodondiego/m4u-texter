/**
 *
 */
package m4u.texter.model.telecom.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import m4u.texter.model.telecom.PhoneOperation;

/**
 * Default implementation for the default methods to handle objects in database
 *
 * @author dinhego
 *
 */
public class DefaultHandlingMethods<T extends PhoneOperation> implements DatabaseOperations<T> {

	/**
	 * Database access
	 */
	@Autowired
	private SessionFactory sessionFactory;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * m4u.texter.model.telecom.dao.DatabaseOperations#persist(m4u.texter.model.
	 * telecom.PhoneOperation)
	 */
	@Override
	public void persist(final T entity) {

		this.sessionFactory.getCurrentSession().persist(entity);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * m4u.texter.model.telecom.dao.DatabaseOperations#update(m4u.texter.model.
	 * telecom.PhoneOperation)
	 */
	@Override
	public void update(final T entity) {

		this.sessionFactory.getCurrentSession().update(entity);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * m4u.texter.model.telecom.dao.DatabaseOperations#findById(java.lang.Long)
	 */
	@Override
	public T findById(final Integer id) {

		if (id == null || id <= 0) {
			throw new InvalidParameterException(String.format("The id for search is null or less than 1: %s", id));
		}

		final Object possibleProduct = this.sessionFactory.getCurrentSession().get(this.getGenericType(), id);

		if (possibleProduct instanceof PhoneOperation) {

			return this.getGenericType().cast(possibleProduct);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see m4u.texter.model.telecom.dao.DatabaseOperations#findAll()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAll() {

		final Session currentSession = this.sessionFactory.getCurrentSession();

		final Criteria criteriaToAllObjects = currentSession.createCriteria(this.getGenericType());

		final List<?> temp = criteriaToAllObjects.list();

		// simple check
		return (List<T>) temp.parallelStream().filter(u -> u instanceof PhoneOperation).collect(Collectors.toList());

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * m4u.texter.model.telecom.dao.DatabaseOperations#delete(m4u.texter.model.
	 * telecom.PhoneOperation)
	 */
	@Override
	public void delete(final T entity) {
		if (entity != null) {
			this.sessionFactory.getCurrentSession().delete(entity);
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see m4u.texter.model.telecom.dao.DatabaseOperations#deleteAll()
	 */
	@Override
	public void deleteAll() {
		// README not to be implemented

	}

	// getters and setters

	@SuppressWarnings("unchecked")
	private Class<T> getGenericType() {
		final ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
		final Type superClassTypes = parameterizedType.getActualTypeArguments()[0];

		// TODO refactor!

		return (Class<T>) superClassTypes;
	}

	public void setSessionFactory(final SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
