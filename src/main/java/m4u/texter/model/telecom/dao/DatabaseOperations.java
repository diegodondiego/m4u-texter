/**
 *
 */
package m4u.texter.model.telecom.dao;

import java.util.List;

import m4u.texter.model.telecom.PhoneOperation;

/**
 * interface to define the default database operations
 *
 * @author dinhego
 *
 */
public interface DatabaseOperations<T extends PhoneOperation> {

	void persist(T entity);

	void update(T entity);

	T findById(Integer id);

	void delete(T entity);

	List<T> getAll();

	void deleteAll();

}
