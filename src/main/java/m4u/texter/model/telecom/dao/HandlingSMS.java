/**
 *
 */
package m4u.texter.model.telecom.dao;

import org.springframework.stereotype.Repository;

import m4u.texter.model.telecom.operations.SMS;

/**
 * DAO for handling SMS in the database
 *
 * @author dinhego
 *
 */
@Repository
public class HandlingSMS extends DefaultHandlingMethods<SMS> {

}
