/**
 *
 */
package m4u.texter.model.telecom.sms;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import m4u.texter.model.telecom.RequestResult;
import m4u.texter.model.telecom.TelecomServices;
import m4u.texter.model.telecom.dao.HandlingSMS;
import m4u.texter.model.telecom.exceptions.TelecomServicesException;
import m4u.texter.model.telecom.operations.SMS;
import m4u.texter.model.telecom.operations.ValidationResult;

/**
 * Service implementation for send SMS.
 *
 * @author dinhego
 *
 */
public class SMSProviderService implements TelecomServices<SMS> {

	private static final Logger LOG = LogManager.getLogger(SMSProviderService.class);

	/**
	 * Current inexistent phone users
	 */
	private static final List<String> INEXISTENT_USERS = Lists.newArrayList("21989898989", "21988887777");

	@Autowired
	private HandlingSMS handlingSMS;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * m4u.texter.model.telecom.TelecomServices#send(m4u.texter.model.telecom.
	 * PhoneOperation)
	 */
	@Override
	@Transactional
	public RequestResult send(final SMS aSMS) throws TelecomServicesException {

		if (aSMS == null) {
			LOG.error("attempt to send a null SMS.");
			return new RequestResult(false, ValidationResult.INTERNAL_ERROR, "INVALID (NULL) SMS");
		}

		RequestResult result;

		// check if the one of the users doesn't exist
		final String cleansedSenderNumber = aSMS.getSenderNumber().replace("(", EMPTY).replace(")", EMPTY)
				.replaceAll("\\s", EMPTY);
		if (INEXISTENT_USERS.contains(cleansedSenderNumber)) {
			return new RequestResult(false, ValidationResult.MOBILE_USER_NOT_FOUND, "SENDER MOBILE USER NOT FOUND");
		}

		final String cleansedDestinationNumber = aSMS.getDestinationNumber().replace("(", EMPTY).replace(")", EMPTY)
				.replaceAll("\\s", EMPTY);
		if (INEXISTENT_USERS.contains(cleansedDestinationNumber)) {
			return new RequestResult(false, ValidationResult.MOBILE_USER_NOT_FOUND,
					"DESTINATION MOBILE USER NOT FOUND");
		}

		// save the sms in the database
		try {
			this.handlingSMS.persist(aSMS);
		} catch (final Throwable t) {
			LOG.error(format("error while saving this sms at db: [%s].", aSMS.toString()), t);
			return new RequestResult(false, ValidationResult.INTERNAL_ERROR,
					format("error while saving at database: %s", t.getMessage()));
		}

		result = new RequestResult(true, ValidationResult.OK, format("MESSAGE WITH ID [%s] SENT", aSMS.getId()));

		return result;
	}

	/**
	 *
	 * @return all the SMS sent with success by this service
	 */
	@Transactional
	public List<SMS> getAll() {
		return this.handlingSMS.getAll();
	}

	/**
	 *
	 * @param id
	 *            a unique sms id
	 * @return a {@link SMS} with the id informed as parameter,
	 *         <code>null</code> if there isn't any {@link SMS} with this id
	 */
	@Transactional
	public SMS getById(final Integer id) {
		return this.handlingSMS.findById(id);
	}

	public void setHandlingSMS(final HandlingSMS handlingSMS) {
		this.handlingSMS = handlingSMS;
	}

}
