/**
 *
 */
package m4u.texter.model.telecom;

import m4u.texter.model.telecom.exceptions.TelecomServicesException;

/**
 * Interface with basic operations defining a telecom service
 *
 * @author dinhego
 *
 */
public interface TelecomServices<T extends PhoneOperation> {

	/**
	 * Defines the basic service of send a phone operation.
	 *
	 * @param operation
	 * @return the result of the send operation
	 * @throws TelecomServicesException
	 */
	RequestResult send(T operation) throws TelecomServicesException;

}
