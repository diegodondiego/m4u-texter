/**
 *
 */
package m4u.texter.model.telecom;

import com.google.gson.GsonBuilder;

import m4u.texter.model.telecom.operations.ValidationResult;

/**
 * Contains a user request result
 *
 * @author dinhego
 *
 */
public class RequestResult {

	private final boolean sucess;

	private final String message;

	private final ValidationResult validationResult;

	public RequestResult(final boolean hadSucess, final ValidationResult validationResult, final String message) {
		super();
		this.sucess = hadSucess;
		this.validationResult = validationResult;
		this.message = message;

	}

	public boolean isSucess() {
		return this.sucess;
	}

	public String getMessage() {
		return this.message;
	}

	public ValidationResult getValidationResult() {
		return this.validationResult;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new GsonBuilder().setPrettyPrinting().create().toJson(this);
	}

}
