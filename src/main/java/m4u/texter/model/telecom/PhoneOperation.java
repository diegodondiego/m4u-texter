/**
 *
 */
package m4u.texter.model.telecom;

import java.io.Serializable;

import m4u.texter.model.telecom.exceptions.OperationValidationError;

/**
 * Defines a possible phone operation.
 *
 * @author dinhego
 *
 */
public interface PhoneOperation extends Serializable {

	Integer getId();

	void validate() throws OperationValidationError;

}
