/**
 *
 */
package m4u.texter.model.telecom.operations;

/**
 * Results enum for the validation of a phone operation
 *
 * @author dinhego
 *
 */
public enum ValidationResult {

	OK(0), INVALID_SENDER(1), INVALID_RECEIVER(2), INVALID_MESSAGE(3), MOBILE_USER_NOT_FOUND(4), INTERNAL_ERROR(5);

	/**
	 * error code
	 */
	private int code;

	private ValidationResult(final int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

}
