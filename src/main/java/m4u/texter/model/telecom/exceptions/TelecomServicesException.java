/**
 *
 */
package m4u.texter.model.telecom.exceptions;

/**
 * General error handler for the telecom services.
 *
 * @author dinhego
 *
 */
public class TelecomServicesException extends Exception {

	private static final long serialVersionUID = -7416465300741791068L;

}
