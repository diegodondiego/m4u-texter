/**
 *
 */
package m4u.texter.model.telecom.exceptions;

import m4u.texter.model.telecom.operations.ValidationResult;

/**
 * An exception throwed when an error occur.
 *
 * @author dinhego
 *
 */
public class OperationValidationError extends Exception {

	private static final long serialVersionUID = 8911416363415296927L;

	private final ValidationResult error;

	public OperationValidationError(final ValidationResult error, final String message) {
		super(message);
		this.error = error;
	}

	public ValidationResult getError() {
		return this.error;
	}

}
