/**
 *
 */
package m4u.texter.controller;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.math.NumberUtils.isNumber;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import m4u.texter.model.telecom.RequestResult;
import m4u.texter.model.telecom.exceptions.OperationValidationError;
import m4u.texter.model.telecom.exceptions.TelecomServicesException;
import m4u.texter.model.telecom.operations.SMS;
import m4u.texter.model.telecom.operations.ValidationResult;
import m4u.texter.model.telecom.sms.SMSProviderService;

/**
 * Jersey REST api component
 *
 * @author dinhego
 *
 */
@Path("/sms")
public class SMSGateway {

	@Autowired
	private SMSProviderService smsProviderService;

	/**
	 * REST endpoint for send a SMS through a phone operator
	 *
	 * @param sendersNumber
	 * @param destinationNumber
	 * @param message
	 * @param expirationDate
	 * @return an {@link Entity} with the result of the query
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response send(@FormParam("senderNumber") final String sendersNumber,
			@FormParam("destinationNumber") final String destinationNumber, @FormParam("message") final String message,
			@FormParam("expirationDate") final String expirationDate) {

		// validate the date
		if (!isEmpty(expirationDate)) {
			final LocalDate parsedExpirationDate;
			try {
				parsedExpirationDate = LocalDate.parse(expirationDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			} catch (final DateTimeParseException e) {
				// create a simple json with a message
				return Response.status(Status.METHOD_NOT_ALLOWED)
						.entity(Entity.json(new ValidationMessage(format(
								"Validation exception! Date not in right format [dd/MM/yyyy]: [%s]", expirationDate))))
						.build();
			}

			// if date less than now, throw error
			if (LocalDate.now().isAfter(parsedExpirationDate)) {
				return Response.status(Status.METHOD_NOT_ALLOWED).entity(Entity.json(
						new ValidationMessage(format("Validation exception! Date expired: [%s]", expirationDate))))
						.build();
			}
		}

		// try to create the SMS pojo, with validations
		SMS sms;
		try {
			sms = new SMS(null, sendersNumber, destinationNumber, message);
		} catch (final OperationValidationError e1) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity(Entity.json(new ValidationMessage(
					format("Validation exception! SMS's fields with problem: [%s]", e1.getMessage())))).build();
		}

		// try to reach the phone operator and send the SMS
		RequestResult smsRequestResult;
		try {
			smsRequestResult = this.smsProviderService.send(sms);
		} catch (final TelecomServicesException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Internal Server Error! Couldn't get response from operator. Check log.").build();
		}

		// final results
		if (!smsRequestResult.isSucess()) {

			// a problem happened. check if the mobile user not found.
			if (ValidationResult.MOBILE_USER_NOT_FOUND.equals(smsRequestResult.getValidationResult())) {
				return Response.status(Status.NOT_FOUND).entity(Entity.json(smsRequestResult)).build();
			}
			// else send as a internal server error
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(Entity.json(smsRequestResult)).build();
		}

		return Response.status(Status.CREATED).entity(Entity.json(smsRequestResult)).build();
	}

	/**
	 *
	 * @return all the SMS's saved
	 */
	@GET
	@Path("getAll")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		return Response.status(Status.OK).entity(Entity.json(this.smsProviderService.getAll())).build();
	}

	/**
	 *
	 * @return a SMS's by the id
	 */
	@GET
	@Path("find/byid/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByID(@PathParam("id") String id) {

		if (isEmpty(id) || !isNumber(id)) {
			return Response.status(Status.METHOD_NOT_ALLOWED)
					.entity(Entity.json(new ValidationMessage(format("Validation exception! Invalid id: [%s]", id))))
					.build();
		}

		return Response.status(Status.OK).entity(Entity.json(this.smsProviderService.getById(Integer.valueOf(id))))
				.build();
	}

	/**
	 * simple pojo to send an error message to user
	 *
	 * @author dinhego
	 *
	 */
	public class ValidationMessage {

		private final String error;

		public ValidationMessage(String error) {
			super();
			this.error = error;
		}

		public String getError() {
			return this.error;
		}

	}
}
