/**
 *
 */
package m4u.texter.controller.helpers.jersey;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;

import m4u.texter.controller.SMSGateway;

/**
 * Class to load the web application configuration
 *
 * @author dinhego
 *
 */
public class TexterContextLoader extends ResourceConfig {

	/**
	 *
	 */
	public TexterContextLoader() {
		this.register(RequestContextFilter.class);
		this.register(SMSGateway.class);
	}

}
