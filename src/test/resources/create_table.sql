create schema if not exists "M4U";

create table if not exists M4U.SMS (id number not null primary key auto_increment, senderNumber varchar(255) not null, destinationNumber varchar(255) not null, message varchar(255) not null);