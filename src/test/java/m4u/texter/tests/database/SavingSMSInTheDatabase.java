/**
 *
 */
package m4u.texter.tests.database;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import m4u.texter.model.telecom.dao.HandlingSMS;
import m4u.texter.model.telecom.exceptions.OperationValidationError;
import m4u.texter.model.telecom.operations.SMS;

/**
 * Saving SMSs in database
 *
 * @author dinhego
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:beans.xml" })
@Transactional
public class SavingSMSInTheDatabase {

	@Autowired
	private HandlingSMS handlingSMS;

	/**
	 * just check if the dao was autowired
	 */
	@Test
	public void testingWiredDependency() {
		assertTrue(this.handlingSMS != null);
	}

	/**
	 * save a single SMS and checks if the id was setted
	 *
	 * @throws OperationValidationError
	 *
	 */
	@Test
	public void savingASMS() throws OperationValidationError {
		final SMS sms = new SMS(null, "(21) 998765432", "21 987654321", "teste");
		this.handlingSMS.persist(sms);

		assertTrue(sms.getId() > 0);
	}
}
