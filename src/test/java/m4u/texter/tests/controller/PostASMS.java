/**
 *
 */
package m4u.texter.tests.controller;

import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import m4u.texter.controller.SMSGateway;

/**
 * Tests for the {@link SMSGateway}.
 *
 * @author dinhego
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:beans.xml" })
@Transactional
public class PostASMS {

	/**
	 * Is a simple bean at beans configuration.
	 */
	@Autowired
	private SMSGateway smsGateway;

	/**
	 * Testing if the spring DI is working.
	 */
	@Test
	public void areYouSetted() {

		assertTrue(this.smsGateway != null);
	}

	/**
	 * Setting a expired date to the SMS
	 */
	@Test
	public void withExpiredDate() {
		final String yesterday = LocalDate.now().atStartOfDay().minusDays(1)
				.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

		final Response result = this.smsGateway.send(null, null, null, yesterday);

		assertTrue(Status.METHOD_NOT_ALLOWED.equals(result.getStatusInfo()));
	}

	/**
	 * Setting a inexistent user based on field inexistent users on the sms
	 * provider service; i.e., this numbers: "21989898989", "21988887777"
	 */
	@Test
	public void withInexistentUser() {

		Response result = this.smsGateway.send("(21) 989898989", "21 987654321", "tests for inexistent user", null);

		assertTrue(Status.NOT_FOUND.equals(result.getStatusInfo()));

		result = this.smsGateway.send("(21) 987654321", "21 988887777", "tests for inexistent user", null);

		assertTrue(Status.NOT_FOUND.equals(result.getStatusInfo()));
	}

	/**
	 * Testing invalid SMS: invalid numbers, invalid message
	 */
	@Test
	public void withInvalidSMS() {

		// wrong sender number
		Response result = this.smsGateway.send(null, "21 987654321", "invalid SMS", null);
		assertTrue(Status.METHOD_NOT_ALLOWED.equals(result.getStatusInfo()));

		result = this.smsGateway.send("", "21 987654321", "invalid SMS", null);
		assertTrue(Status.METHOD_NOT_ALLOWED.equals(result.getStatusInfo()));

		// wrong destination number
		result = this.smsGateway.send("21 987654321", null, "invalid SMS", null);
		assertTrue(Status.METHOD_NOT_ALLOWED.equals(result.getStatusInfo()));

		result = this.smsGateway.send("21 987654321", "", "invalid SMS", null);
		assertTrue(Status.METHOD_NOT_ALLOWED.equals(result.getStatusInfo()));

		// invalid message
		// empty and null
		result = this.smsGateway.send("21 987654321", "21 984654321", "", null);
		assertTrue(Status.METHOD_NOT_ALLOWED.equals(result.getStatusInfo()));

		result = this.smsGateway.send("21 987654321", "21 984654321", null, null);
		assertTrue(Status.METHOD_NOT_ALLOWED.equals(result.getStatusInfo()));

		// message bigger than 160 characters
		result = this.smsGateway.send("21 987654321", "21 984654321",
				new BigInteger(1610, new SecureRandom()).toString(32), null);
		assertTrue(Status.METHOD_NOT_ALLOWED.equals(result.getStatusInfo()));
	}

	/**
	 * tests with valid info
	 */
	public void withValidInfo() {

		// no date informed
		Response result = this.smsGateway.send("21 987654321", "21 984654321", "teste", null);
		assertTrue(Status.CREATED.equals(result.getStatusInfo()));

		// with date informed
		final String tomorrow = LocalDate.now().atStartOfDay().plusDays(1)
				.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

		result = this.smsGateway.send("21 987654321", "21 984654321", "teste", tomorrow);
		assertTrue(Status.CREATED.equals(result.getStatusInfo()));
	}
}
