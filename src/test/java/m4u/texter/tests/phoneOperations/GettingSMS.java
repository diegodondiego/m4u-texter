/**
 *
 */
package m4u.texter.tests.phoneOperations;

import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import m4u.texter.model.telecom.RequestResult;
import m4u.texter.model.telecom.exceptions.OperationValidationError;
import m4u.texter.model.telecom.exceptions.TelecomServicesException;
import m4u.texter.model.telecom.operations.SMS;
import m4u.texter.model.telecom.sms.SMSProviderService;

/**
 * tests the sms get
 *
 * @author dinhego
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:beans.xml" })
@Transactional
public class GettingSMS {

	@Autowired
	private SMSProviderService smsProviderService;

	/**
	 * just save some sms's on db
	 *
	 * @throws OperationValidationError
	 * @throws TelecomServicesException
	 */
	private void saveSomeSMS() throws TelecomServicesException, OperationValidationError {

		RequestResult result = this.smsProviderService.send(new SMS(null, "21 987654321", "21 984654321", "teste"));
		assertTrue(result.isSucess());

		result = this.smsProviderService.send(new SMS(null, "21 987654321", "21 984654321", "teste"));
		assertTrue(result.isSucess());
	}

	/**
	 * get all the saved sms. at least the number of sms sent in the method
	 * above.
	 *
	 * @throws OperationValidationError
	 * @throws TelecomServicesException
	 *
	 */
	@Test
	public void getAll() throws TelecomServicesException, OperationValidationError {

		this.saveSomeSMS();

		final List<SMS> all = this.smsProviderService.getAll();

		assertTrue(all != null && all.size() >= 2);
	}

	/**
	 * get a sms by id.
	 *
	 * @throws OperationValidationError
	 * @throws TelecomServicesException
	 *
	 */
	@Test
	public void getByID() throws TelecomServicesException, OperationValidationError {

		this.saveSomeSMS();

		final Integer idToBeFound = this.smsProviderService.getAll().stream().findFirst().get().getId();

		final SMS theFirstSMS = this.smsProviderService.getById(idToBeFound);

		assertTrue(theFirstSMS != null && theFirstSMS.getId().equals(idToBeFound));
	}

}
