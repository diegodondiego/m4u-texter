/**
 *
 */
package m4u.texter.tests.phoneOperations;

import static org.junit.Assert.assertTrue;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import m4u.texter.model.telecom.RequestResult;
import m4u.texter.model.telecom.exceptions.OperationValidationError;
import m4u.texter.model.telecom.exceptions.TelecomServicesException;
import m4u.texter.model.telecom.operations.SMS;
import m4u.texter.model.telecom.operations.ValidationResult;
import m4u.texter.model.telecom.sms.SMSProviderService;

/**
 * Testing the SMS provider
 *
 * @author dinhego
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:beans.xml" })
@Transactional
public class SendASMS {

	@Autowired
	private SMSProviderService smsProviderService;

	/**
	 * sending a null as parameter
	 *
	 * @throws TelecomServicesException
	 */
	@Test
	public void nullSMS() throws TelecomServicesException {

		final RequestResult result = this.smsProviderService.send(null);
		assertTrue(null != result && !result.isSucess());
	}

	/**
	 *
	 * Try to send a sms with invalid users (inexistent users)
	 *
	 * @throws OperationValidationError
	 * @throws TelecomServicesException
	 *
	 */
	@Test
	public void smsWithInexistentUser() throws OperationValidationError, TelecomServicesException {
		// sms with an inexistent sender number
		SMS sms = new SMS(null, "(21) 989898989", "21 987654321", "teste");
		this.smsProviderService.send(sms);
		RequestResult result = this.smsProviderService.send(sms);
		assertTrue(String.format("SMS with sender mobile user not found: %s", sms), null != result && !result.isSucess()
				&& ValidationResult.MOBILE_USER_NOT_FOUND.equals(result.getValidationResult()));

		// sms with an inexistent destination number
		sms = new SMS(null, "(21) 987654321", "21 988887777", "teste");
		this.smsProviderService.send(sms);
		result = this.smsProviderService.send(sms);
		assertTrue(String.format("SMS with destination mobile user not found: %s", sms), null != result
				&& !result.isSucess() && ValidationResult.MOBILE_USER_NOT_FOUND.equals(result.getValidationResult()));
	}

	/**
	 * sending a simple SMS
	 *
	 * @throws OperationValidationError
	 * @throws TelecomServicesException
	 */
	@Test
	public void validSMS() throws OperationValidationError, TelecomServicesException {

		final SMS sms = new SMS(null, "(21) 998765432", "21 987654321", "teste");
		final RequestResult result = this.smsProviderService.send(sms);
		assertTrue(null != result && result.isSucess());
	}

}
