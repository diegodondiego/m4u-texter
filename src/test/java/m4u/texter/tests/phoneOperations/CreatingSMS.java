/**
 *
 */
package m4u.texter.tests.phoneOperations;

import static java.lang.String.format;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.junit.Test;

import m4u.texter.model.telecom.exceptions.OperationValidationError;
import m4u.texter.model.telecom.operations.SMS;

/**
 * Testing the validation of a {@link SMS}
 *
 * @author dinhego
 *
 */
public class CreatingSMS {

	/**
	 * try to create a SMS with null sender.
	 *
	 * @throws OperationValidationError
	 */
	@Test(expected = OperationValidationError.class)
	public void creatingASMSWithNullFrom() throws OperationValidationError {

		final SMS sms = new SMS(null, null, "23232323", "teste");
		fail(format("sms created without the from user number: [%s]", sms));
	}

	/**
	 * try to create a SMS with null receiver.
	 *
	 * @throws OperationValidationError
	 */
	@Test(expected = OperationValidationError.class)
	public void creatingASMSWithNullTo() throws OperationValidationError {

		final SMS sms = new SMS(null, "23232323", null, "teste");
		fail(format("sms created without the from user number: [%s]", sms));
	}

	/**
	 * try to create a SMS with null sender.
	 *
	 * @throws OperationValidationError
	 */
	@Test(expected = OperationValidationError.class)
	public void creatingASMSWithEmptyFrom() throws OperationValidationError {

		final SMS sms = new SMS(null, "", "23232323", "teste");
		fail(format("sms created without the from user number: [%s]", sms));
	}

	/**
	 * try to create a SMS with null receiver.
	 *
	 * @throws OperationValidationError
	 */
	@Test(expected = OperationValidationError.class)
	public void creatingASMSWithEmptyTo() throws OperationValidationError {

		final SMS sms = new SMS(null, "23232323", "", "teste");
		fail(format("sms created without the from user number: [%s]", sms));
	}

	/**
	 * try to create a SMS with valid numbers using the whitelist approach.
	 *
	 * @throws OperationValidationError
	 */
	@Test
	public void creatingASMSWithValidToAndFrom() throws OperationValidationError {

		// this first test is using the pattern (xx) xxxxxxxxx
		SMS sms = new SMS(null, "(21) 981234567", "(21) 987654321", "teste");
		assertTrue(sms != null);

		// this test is using the pattern xx xxxxxxxxx
		sms = new SMS(null, "21 981234567", "21 987654321", "teste");
		assertTrue(sms != null);

		// same two tests with spaces in the start and in the end
		sms = new SMS(null, "(21) 981234567 ", " (21) 987654321", "teste");
		assertTrue(sms != null);

		sms = new SMS(null, " 21 981234567", "21 987654321 ", "teste");
		assertTrue(sms != null);

		// with spaces in start and end
		sms = new SMS(null, " 21 981234567     ", "    21 987654321 ", "teste");
		assertTrue(sms != null);
	}

	/**
	 * Testing the empty message validation
	 *
	 * @throws OperationValidationError
	 */
	@Test(expected = OperationValidationError.class)
	public void creatingASMSWithEmptyMessage() throws OperationValidationError {

		final SMS sms = new SMS(null, "(21) 981234567", "(21) 987654321", "");
		fail(format("a sms with empty message were created: [%s]", sms));
	}

	/**
	 * Testing the null message validation
	 *
	 * @throws OperationValidationError
	 */
	@Test(expected = OperationValidationError.class)
	public void creatingASMSWithNullMessage() throws OperationValidationError {

		final SMS sms = new SMS(null, "(21) 981234567", "(21) 987654321", null);
		fail(format("a sms with null message were created: [%s]", sms));
	}

	/**
	 * Testing the message validation for more than 160 chars
	 *
	 * @throws OperationValidationError
	 */
	@Test(expected = OperationValidationError.class)
	public void creatingASMSWithMessageWithMoreThan160Chars() throws OperationValidationError {

		final SMS sms = new SMS(null, "(21) 981234567", "(21) 987654321",
				new BigInteger(1610, new SecureRandom()).toString(32));
		fail(format("a sms with big message were created: [%s]", sms));
	}

}
