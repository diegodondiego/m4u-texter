# README #

### What is this repository for? ###

* Quick summary
> Project of a enterprise SMS provider. It's a technical challenge for a job opportunity at M4U. (:
> According to [wikipedia](https://www.wikiwand.com/en/Text_messaging), *The sender of a text message is commonly referred to as a texter.*
> This project provides a REST API to a enterprise SMS Gateway.
* Version
> 0.0.1

### How do I use the API? ###

* All the requests results is using a **JSON** of the object ``javax.ws.rs.client.Entity`` and the result of the operation is in the **entity** field of this object.
* Sending a SMS
  * use a POST request in the API through **/api/v1/sms** using the parameters:
    * *senderNumber*: the user phone number who will sent the SMS
    * *destinationNumber*: the phone number of the  destination of the SMS
    * *message*: the message that will be sent by the SMS
    * *expirationDate*: a simple date defining the expiration date for the SMS
  * parameters validation rules:
    * *senderNumber* and *destinationNumber* should be a ``java.lang.String`` that matches the regex ``(?:\([1-9]\d\)|[1-9]\d)\s*9[6-9][0-9]{7}`` compile in Java
    * *message* is also a ``java.lang.String`` that can't be ``null``, **empty** or **have more than 160 characters**
    * *expirationDate* should be a ``java.lang.String`` representing a date in the format *dd/MM/yyyy*
  * there is a form to help send the SMS located at **/texter/index.html**
  * the possible responses are:
    * 201: Sms sent
    * 404: Mobile User not found
    * 405: Validation exception
    * 500: Internal Server Error
  * there is two mock phone numbers that can return 'mobile user not found': **21 989898989** and **21 988887777**
* Getting all the SMS
  * use a GET request in the API through **/api/v1/sms/getAll** that returns all SMS sent in this session
* Getting a SMS by id
  * use a GET request in the API through **/api/v1/sms/find/byid/${id}** where **${id}** is the id of a SMS sent

### How do I get set up? ###

* Summary of set up
> everything is set up by the deployment.
> the app is under _**/texter/**_ context when deployed.
> techonologies used:
> - **Java SE 8** -> using lambda, nIO, etc...
> - **Java EE 7** -> using JSON API, REST API
> - **Jersey** 2 -> for REST
> - **Spring Framework 4** -> DI, IoC
> - **Hibernate 4** -> JPA integration
> - **H2 Database (in-memory)**
* Configuration
> the project is under **maven management**
* Dependencies
> see **pom.xml**
* Database configuration
> automatic, through a datasource bean, instantiated by Spring
* How to run tests
> _mvn test_
* Deployment instructions
> it was tested on **tomcat 8.0.5**

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
> me @diegodondiego
* Other community or team contact
